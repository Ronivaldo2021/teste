@extends('layouts.template')
@section('title', 'Criar Produto')
@section('content')
<div class="container mt-4">
    <form method="POST" action="{{route('clientes.insert')}}">
        @csrf

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">CPF</label>
                    <input type="text" class="form-control" id="" name="cpf">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">E-mail</label>
                    <input type="text" class="form-control" id="" name="email">
                </div>
            </div>
            
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Endereço</label>
            <textarea class="form-control" id="" name="endereco" rows="3"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>
@endsection