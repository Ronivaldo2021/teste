<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::orderby('id', 'desc')->paginate();
        return view('clientes.index', ['clientes' => $clientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = new Cliente();
        $cliente->nome = $request->nome;
        $cliente->cpf = $request->cpf;
        $cliente->email = $request->email;
        $cliente->endereco = $request->endereco;
        $cliente->save();
        return redirect()->route('clientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cliente)
    {
        $clientes = Cliente::find($cliente);
        //$clientes->all();
        //dd($clientes);
        return view('clientes.edit', ['clientes' => $clientes]);  
    }

    public function editar(Request $request, Cliente $clientes){
        
      
        $clientes->nome = $request->nome;
        $clientes->cpf = $request->cpf;
        $clientes->email = $request->email;
        $clientes->endereco = $request->endereco;
        $clientes->save();
        return redirect()->route('clientes');
        }

      

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(cliente $cliente)
    {
        $cliente->delete();
        return redirect()->route('clientes');
    }

    public function modal($id){
        $clientes = new Cliente();
        $clientes = $clientes::orderby('id', 'desc')->paginate();
        return view('clientes.index', ['clientes' => $clientes, 'id' => $id]);

     }
}
