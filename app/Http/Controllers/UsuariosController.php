<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuariosController extends Controller
{
    public function login(Request $request){
        $usuario = new Usuario();
        $email = $request->email;
        $senha = $request->senha;

        $usuarios = $usuario::where('email', '=', $email)->where('senha', '=', $senha)->first();
        
        if(@$usuarios->id != null){
            @session_start();
            $_SESSION['id_usuario'] = $usuarios->id;
            $_SESSION['nome_usuario'] = $usuarios->nome;
            $_SESSION['nivel_usuario'] = $usuarios->nivel;
            
            if($_SESSION['nivel_usuario'] == 'admin'){
                return view('clientes.create');
            }else{
                $cliente = new Cliente();
                $clientes = $cliente::orderby('id', 'desc')->paginate();
                return view('clientes.index', ['clientes' => $clientes]);
            }
            
            
    	    

        }else{
            echo "<script language='javascript'> window.alert('Dados Incorretos!') </script>";
            return view('home');
        }
        
       
    }


    public function logout(){
       @session_start();
       @session_destroy();
       return view('home');
    }

}
