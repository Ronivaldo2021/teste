<?php

use App\Http\Controllers\ClienteController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProdutosController;
use App\Http\Controllers\UsuariosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', HomeController::class)->name('home');


Route::post('painel', [UsuariosController::class, 'login'])->name('usuarios.login');

Route::get('/', [UsuariosController::class, 'logout'])->name('usuarios.logout');

Route::get('/clientes', [ClienteController::class, 'index'])->name('clientes');

Route::get('clientes/inserir', [ClienteController::class, 'create'])->name('clientes.inserir');

Route::delete('clientes/{cliente}', [ClienteController::class, 'destroy'])->name('clientes.delete');

Route::post('clientes', [ClienteController::class, 'store'])->name('clientes.insert');

Route::get('clientes/{id}/edit', [ClienteController::class, 'edit'])->name('clientes.edit');

Route::get('clientes/{clientes}/delete', [ClienteController::class, 'modal'])->name('clientes.modal');

Route::put('clientes/{clientes}', [ClienteController::class, 'editar'])->name('clientes.editar');